### Esta funcion me saca de un string cada palabra
### [word1,word2...,wordN] = separar_palabras(string)
def separar_palabras(entrada):
    i=0
    temp_word = ""
    salida = []
    for i in entrada:
        if ((i != " ") and (i !="\n")):
            temp_word += str(i)
        else:
            if temp_word != "":
                salida.append(temp_word)
            temp_word = ""
    if temp_word != "":                ## este tipo de if es para asegurarnos de no meter un "" en la lista
        salida.append(temp_word)      ## esto es para agregar la ultima palabra
    return salida

###FUNCION SUMA
### Z = Func_suma(x,y) siendo Z = x+y
def Func_suma(x,y):
    return int(x)+int(y)

###FUNCION RESTA
### Z = Func_resta(x,y) siendo Z = x-y
def Func_resta(x,y):
    return int(x)-int(y)


if __name__ == "__main__":
    ### CALCULADORA
    ## Parte que pide la practica (sesion de entrenamiento)
    print("{} + {} = {}".format(1,2,Func_suma(1,2)))
    print("{} + {} = {}".format(3,4, Func_suma(3, 4)))
    print("{} - {} = {}".format(5, 6, Func_resta(5, 6)))
    print("{} - {} = {}".format(7, 8, Func_resta(7, 8)))

    ## Parte para practicar
    print("que hago ahora?\n")
    exit = False

    while not exit:
        try:
            entrada = separar_palabras(input("especifica operacion y valores (suma/resta)\n"))
            operacion = entrada[0].lower()
            if operacion == "exit":
                exit = True
                print("adios")
            else:
                v1 = entrada[1]
                v2 = entrada[2]
                if operacion == "suma":
                    print("{} + {} = {}".format(v1, v2, Func_suma(v1, v2)))
                elif operacion == "resta":
                    print("{} - {} = {}".format(v1, v2, Func_resta(v1, v2)))
                else:
                    print("Parece que algo no te termina de quedar claro no?")
        except (ValueError,IndexError):
            print("Es tan sencillo como pone en las intrucciones macho, no es tan dificil...")
        except KeyboardInterrupt:
            print("Adios Machote, ya podias ponerme exit no?")
            exit = True